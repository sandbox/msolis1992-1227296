<?php

/**
 * @author Moises Solis
 *
 * Moises G. Solis
 * Austin, TX, USA
 *
 * This is a composite object.  It contains one settings object and many image objects.
 *
 */

class Simple_Slideshow_Slideshow {
  var $setting;
  var $images = array();
  var $image_count;

  public function __construct() {
    $args = func_get_args();
    $num = func_num_args();

    //error_log("Number of parameters: $num");

    if ($num == 0) {
      $this->setting = new Simple_Slideshow_Setting();
      $this->image_count = 0;

      if (empty($this->setting)) {
        error_log("Sideshow is empty....");
      }
    }
    else {
      $argsStr = '';

      foreach ($args as $arg) {
        $argsStr .= '_' . gettype($arg);
      }

      if (method_exists($this, $constructor = '__construct' . $argsStr)) {
        call_user_func_array(array($this, $constructor), $args);
      }
      else {
        throw new Exception('NO CONSTRUCTOR: ' . get_class() . $constructor, NULL, NULL);
      }
    }
  }

  public function __construct_string($delta) {
    $this->setting = new Simple_Slideshow_Setting($delta);

    $iids = Simple_Slideshow_Image::get_iids($delta);
    foreach ($iids AS $iid) {
      $this->images[] = new Simple_Slideshow_Image($iid);
    }
    $this->image_count = count($this->images);
  }

  public function __get($property) {
    $method = 'get_' . $property;
    if (method_exists($this, $method)) {
      return $this->method;
    }
  }

  public function save() {
    $setting->update();
    foreach ($this->images AS $image){
      $images->update();
    }
  }

  public function add_image(Simple_Slideshow_Image $image) {
    $this->images[] = $image;
  }
  
  public function set_images(array $images) {
    $this->images = $images;
    $this->image_count = count($this->images);
  }

  public function set_setting(Simple_Slideshow_Setting $setting) {
    $this->setting = $setting;
  }

  public function get_setting() {
    return $this->setting;
  }

  public function get_images() {
    return $this->images;
  }
  
  public function get_image_count() {
    return $this->image_count;
  }
  
  public function __toString() {
    $s_setting = $this->setting->__toString();
    
    $output = "Setting[$s_setting] ";
    $output .= "Images[";
    foreach ($images AS $image) {
      $s_image = $image->__toString();
      $output .= "$s_image "; 
    }
    $output .= "]";
    return $output;
  }
}