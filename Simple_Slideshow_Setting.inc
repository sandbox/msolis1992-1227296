<?php

/**
 * @author Moises Solis
 *
 * Moises G. Solis
 * Austin, TX, USA
 *
 * Data structure for this object.   Refer to installer.
 *
 *$setting = array(
 'delta' Set by DATABASE on create.
 'name' => 'First Nature',
 'fx' => 'fade',
 'speed' => 2000,
 'timeout' => 2500,
 'css_id' => 'image-cycle-first',
 'css_class' => 'image-cycle-slideshow',   This needs to be unique to have more than on slideshows on a page.  Refer to jQuery cycle setting in the themeable function.
 );
 */
class Simple_Slideshow_Setting {
  var $delta;
  var $name;
  var $fx;
  var $speed;
  var $timeout;
  var $css_id;
  var $css_class;

  /**
   * Create one instance of an image_cycle_setting object.   There are three ways to construct one of these objects.
   * The first is to pass in a string (delta is the key (Refer to {block} table).   The second way is to pass in a SQL
   * object for conversion into a Simple_Slideshow_Setting object.  The last way is to pass in an array (refer to installer).
   *
   * @param String $name
   * @param int $fx
   * @param int $speed
   * @param int $timeout
   * @param String $css_id
   * @param String $css_class
   */
  public function __construct() {
    $args = func_get_args();
    $num = func_num_args();
    $argsStr = '';

    //error_log("Number of parameters in Simple_Slideshow_Setting : $num");

    if ($num == 0) {
      $this->count++;
      //error_log("Simple_Slideshow_Setting count : $this->count");
      $this->name = 'New Simple Slideshow';
      $this->fx = 'fade';
      $this->speed = 2000;
      $this->timeout = 2500;
      $this->css_id = "simple-slideshow";
      $this->css_class = 'simple-slideshow';
    }
    else {
      foreach ($args as $arg) {
        $argsStr .= '_' . gettype($arg);
      }

      if (method_exists($this, $constructor = '__construct' . $argsStr)) {
        call_user_func_array(array($this, $constructor), $args);
      }
      else {
        throw new Exception('NO CONSTRUCTOR: ' . get_class() . $constructor, NULL, NULL);
      }
    }
  }

  /**
   * Pass in the delta.   This is the key from the {blocK} database table.
   *
   * @param unknown_type $delta
   */
  public function __construct_string($delta) {
    $result = db_query("SELECT * FROM {simple_slideshow_settings} WHERE delta = %d", (int) $delta);

    $object = db_fetch_object($result);
    $this->__construct_object($object);
  }

  public function __construct_object($param) {
    if (!empty($param->delta)) {
      $this->delta = $param->delta;
    }
    $this->name = check_plain($param->name);
    $this->fx = check_plain($param->fx);
    $this->speed = (int) $param->speed;
    $this->timeout = (int) $param->timeout;
    $this->css_id = check_plain($param->css_id);
    $this->css_class = check_plain($param->css_class);
  }

  /**
   * This is called by the installer.
   *
   * @param unknown_type $param
   */
  public function __construct_array($param) {
    $this->name = check_plain($param['name']);
    $this->fx = check_plain($param['fx']);
    $this->speed = (int) $param['speed'];
    $this->timeout = (int) $param['timeout'];
    $this->css_id = check_plain($param['css_id']);
    $this->css_class = check_plain($param['css_class']);
  }


  public function __toString() {
    return "Delta[$this->delta] Name[$this->name] FX[$this->fx]";
  }

  public function get_property($property) {
    return $this->$property;
  }
   
  function set_property($property, $value) {
    if (empty($value)) {
      return;
    }
    $this->$property = $value;
  }


  /**
   * The delta is part of the slideshow_setting.   Provide an accessor to this property.  Refer to Controller.
   */
  public static function get_deltas() {
    $deltas = array();
    $result = db_query("SELECT delta FROM {simple_slideshow_settings}");

    while ($delta = db_fetch_array($result)) {
      $deltas[] = $delta['delta'];
    }

    return $deltas;
  }

  /**
   * Add a row of data into the simple_slideshow_settings table.
   *
   *  $setting = array(
   *  'delta' => Primary Key assigned on insert.
   'name' => 'First Nature',
   'fx' => 'fade',
   'speed' => 2000,
   'timeout' => 2500,
   'css_id' => 'image-cycle-first',
   'css_class' => 'image-cycle-slideshow',
   );
   *
   * @param $name
   * @param $fx
   * @param $speed
   * @param $timeout
   */
  public function add() {
    $delta = $this->delta;
    $name = $this->name;
    $fx = $this->fx;
    $speed = $this->speed;
    $timeout = $this->timeout;
    $css_id = $this->css_id;
    $css_class = $this->css_class;


    $query = db_query("INSERT INTO {simple_slideshow_settings}
            (name, fx, speed, timeout, css_id, css_class)
             VALUES ('%s', '%s', %d, %d, '%s', '%s')", 
    $name, $fx, $speed, $timeout, $css_id, $css_class);

    $result = db_query("SELECT delta FROM {simple_slideshow_settings} WHERE name LIKE '%s'", $name);
    $last_in = db_fetch_object($result);
    $this->delta = $last_in->delta;

    //dsm($delta,"Delta for name: $name");
  }

  public function update() {
    $delta = $this->delta;
    $name = $this->name;
    $fx = $this->fx;
    $speed = $this->speed;
    $timeout = $this->timeout;
    $css_id = $this->css_id;
    $css_class = $this->css_class;

    $sql = "UPDATE {simple_slideshow_settings} SET ";
    $sql .= "name = '%s', ";
    $sql .= "fx = '%s', ";
    $sql .= "speed = %d ,";
    $sql .= "timeout = %d ";
    $sql .= "css_id = %s ,";
    $sql .= "css_class = %s ,";
    $sql .= "WHERE delta = %d";

    $query = db_query($sql, $name, $fx, $speed, $timeout, $css_id, $css_class, $delta);
  }
}