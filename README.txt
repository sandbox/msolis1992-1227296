/**
 * @author Moises Solis
 * 
 * Moises G. Solis
 * Austin, TX, USA
 *
 */
 
The goal of this module is to create and use a simple slideshow of images.   One should be able to make as many as they want on one page.  They should be fairly easy to configure and use.   Nothing fancy - Keep it simple.  There are other modules: slideshow, image_cycle, etc., worth looking at.   All use the wonderfull jQuery Cycle plugin.  One day, all of these modules may be merged with full support of taxonomy, views, and image manipulation.   Each module has a slightly different focus as is worth studing.

Each slideshow has a configuration form that spans three pages.   The first form allows one to create or modify an existing slideshow.   The next form allows one to modify the settings of that slideshow.   This includes the transitions, speed, size, and CSS properties that pertain to that slideshow instance.   The last form allows one to enter or modify the images of the slideshow.

This module assumes that you have placed your images on your Drupal site.   There are many ways of doing that.    

One will need to move their images up to the server.  Once there,
a slideshow may be created.   The slideshow should have a name.   This name should be unique amongst all slideshows.
The slideshow will have settings that determine the transition type, speed, etc.   Each slideshow will then have one or more
images.

To help use this module, I have included three small images, that I created, own, and share, and created a slideshow that uses
those images.   One may create blocks or attach the slideshow to a node or theme.   There is a theme function that will
render a slideshow.   Call this function as needed.   

Assumptions:
1. You will need to use the jquery_plugin module.   That module provides the JQuery Cycle plugin.
2. The people that create and configure slideshows will want to use them in blocks and in node content.
3. One can create as many slideshows as one needs.
4. One can add as many images to a slideshow as one wants.
5. It is assumed that the images are on your server.   How they get there is up to you.  Me, I use the image, image_cache module to place the images into specific folders.   Before I insert the images, I set the path to the folder.   That way I can manage the images.
6. Only someone with administrator priviledge can use the forms.   This simplifies validation.
   
1. Create a block.
2. Enter the name of the images to cycle through.
3. Specify any and all parameters.
4. Deploy it.