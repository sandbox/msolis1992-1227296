<?php
 /**
 * @author Moises Solis
 * 
 * Moises G. Solis
 * Austin, TX, USA
 *
 * This is a composite object.  It contains all of the slideshow objects.  
 * Each slideshow contains a settings object and many image objects.   The 
 * objects are static.   This uses a Singleton pattern.
 * 
 *
 */

/** Load all the support include files. */
module_load_include('inc', 'simple_slideshow', 'Simple_Slideshow_Slideshow');
module_load_include('inc', 'simple_slideshow', 'Simple_Slideshow_Image');  
module_load_include('inc', 'simple_slideshow', 'Simple_Slideshow_Setting');

/**
 * This definition is used to load the other objects in this module.  
 * This function may conflict with others of the same name defined in other modules.
 * Instead, load the needed include files manually.
 * 
 * 
 * @param unknown_type $class_name
 */
/*
function __autoload($class_name) {
  module_load_include ('inc', 'simple_slideshow', $class_name);
}
*/

class Simple_Slideshow_Controller {
  private static $instance;
  private $slideshows = array();

  /**
   * Singleton pattern.  Prepare static slideshows array.
   */
  private function __construct() {
    $deltas = Simple_Slideshow_Setting::get_deltas();

    foreach ($deltas AS $delta) {
      $this->slideshows[$delta] = new Simple_Slideshow_Slideshow($delta);
    }
  }
  
  /**
   * Static access to all slideshows.
   */
  public static function getController() {
    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c;
    }

    return self::$instance;
  }

  /**
   * Call this function to reset the static slideshows.  It will query the database to
   * restore the latest settings from the db.  Normally this is only run while performing
   * configuration changes.
   */
  public function reset() {
    unset(self::$instance);
    return self::$getController();
  }
  
  /**
   * Called by the configuration pages.
   */
  public function get_all_slideshows() {
    return $this->slideshows;
  }
 
  /**
   * This is the same as the delta in the block database table,
   * 
   * @param unknown_type $delta
   */
  public function get_slideshow($delta) {
    return $this->slideshows[$delta];
  }
}