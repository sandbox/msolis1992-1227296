<?php

/**
 * @author Moises Solis
 *
 * Moises G. Solis
 * Austin, TX, USA
 *
 *This is the database structure for this object.  Refer to the installer.
 *
 *$image = array (
 'delta' => $delta,
 'image_name' => 'Highrise in the country',
 'image_src' => $path . '/images/IMG_2881.jpg',
 'image_class' => 'image-cycle-image',
 'image_alt' => 'An image of a tall single plant',
 'width' => 50,
 'height' => 300,
 'weight' => 200,
 'is_active' => TRUE,
 'link_url' => 'http://msolis.com',
 );
 */
class Simple_Slideshow_Image {
  var $iid; //This is set by the db.
  var $delta;
  var $image_name;
  var $image_src;
  var $image_class;
  var $image_alt;
  var $width;
  var $height;
  var $weight;
  var $is_active;
  var $link_url;

  public function __construct() {
    $args = func_get_args();
    $num = func_num_args();
    $argsStr = '';

    if ($num == 0) {
    }
    else {

      foreach ($args as $arg) {
        $argsStr .= '_' . gettype($arg);
      }

      if (method_exists($this, $constructor = '__construct' . $argsStr)) {
        call_user_func_array(array($this, $constructor), $args);
      }
      else {
        throw new Exception('NO CONSTRUCTOR: ' . get_class() . $constructor, NULL, NULL);
      }
    }
  }

  /**
   * Build the image object based on the key - iid.
   * @param String $iid
   */
  public function __construct_string($iid) {
    $result = db_query("SELECT * FROM {simple_slideshow_images} WHERE iid = %d", (int) $iid);
    $object = db_fetch_object($result);
    $this->__construct_object($object);
  }
  
  public function __toString() {
    return "Name[$this->image_name] SRC[$this->image_src] ALT[$this->image_alt]";
  }

  /**
   * This acts like a clone.  Takes a MySQL object and converts it to an Simple_Slideshow_image object.
   * @param Object $object
   */
  public function __construct_object($object) {
    // The delta value will not be know by the form
    // when a new setting is created.
    if (!empty($object->iid)) {
      $this->iid = $object->iid;
    }

    $this->delta = (int) $object->delta;
    $this->image_name = check_plain($object->image_name);
    $this->image_src = check_plain($object->image_src);
    $this->image_class = check_plain($object->image_class);
    $this->image_alt = check_plain($object->image_alt);
    $this->weight = (int) $object->weight;
    $this->width = (int) $object->width;
    $this->height = (int) $object->height;
    $this->is_active = ($object->is_active == 'TRUE') ? TRUE : FALSE;
    $this->link_url = check_plain($object->link_url);
  }

  /**
   * This allow for the creation of Simple_Slideshow_image objects using arrays.  Refer to the installer.
   *
   * @param unknown_type $object
   */
  public function __construct_array($object) {
    if (is_array($object)) {
      if (!empty($object['iid'])) {
        $this->iid = $object['iid'];
      }

      $this->delta = (int) $object['delta'];
      $this->image_name = check_plain($object['image_name']);
      $this->image_src = check_plain($object['image_src']);
      $this->image_class = check_plain($object['image_class']);
      $this->image_alt = check_plain($object['image_alt']);
      $this->weight = (int) $object['weight'];
      $this->width = (int) $object['width'];
      $this->height = (int) $object['height'];
      $this->is_active = ($object['is_active'] == 'TRUE') ? TRUE : FALSE;
      $this->link_url = check_plain($object['link_url']);
    }
  }

  /**
   * Normal getter.  Pass in the property name.
   *
   * @param $property
   */
  public function get_property($property) {
    return $this->$property;
  }
   
  /**
   * Normal setter.
   *
   * @param String $property
   * @param Varies $value
   */
  public function set_property($property, $value) {
    if (empty($value)) {
      return;
    }
    $this->$property = $value;
  }

  /**
   * Return an array of keys to all images based on their delta value.   Refer to Slideshow object.
   *
   * @param String $delta
   */
  public static function get_iids($delta) {
    $iids = array();
    $result = db_query(db_rewrite_sql("SELECT iid FROM {simple_slideshow_images} WHERE delta = %d"), $delta);

    while ($iid = db_fetch_array($result)) {
      $iids[] = $iid['iid'];
    }

    return $iids;
  }

  /**
   * Defualt database update.
   */
  public function update() {
    $iid = $this->iid;
    $delta = $this->delta;
    $image_name = $this->image_name;
    $image_src = $this->image_src;
    $image_class = $this->image_class;
    $image_alt = $this->image_alt;
    $weight = $this->weight;
    $width = $this->width;
    $height = $this->height;
    $is_active = ($this->is_active) ? 'TRUE' : 'FALSE';
    $link_url = $this->link_url;

    $sql  = "UPDATE {simple_slideshow_images} SET ";
    $sql .= "delta = %d, ";
    $sql .= "image_id = '%s', ";
    $sql .= "image_name = '%s', ";
    $sql .= "image_src = '%s', ";
    $sql .= "image_class = '%s', ";
    $sql .= "image_alt = '%s', ";
    $sql .= "weight = %d, ";
    $sql .= "width = %d, ";
    $sql .= "height = %d, ";
    $sql .= "is_active = '%s', ";
    $sql .= "link_id = '%s', ";
    $sql .= "link_url = '%s', ";
    $sql .= "link_title = '%s', ";
    $sql .= "link_target = '%s', ";
    $sql .= "link_class = '%s' ";
    $sql .= "WHERE iid = %d ";

    $query = db_query($sql, $delta, $image_id, $image_name,
    $image_src, $image_class, $image_alt, $weight,
    $width, $height, $is_active, $link_id, $link_url,
    $link_title, $link_target, $link_class, $iid);
  }

  /**
   * Default database adder. Set iid value after inserting the record.
   */
  public function add() {
    $delta = $this->delta;
    $image_name = $this->image_name;
    $image_src = $this->image_src;
    $image_class = $this->image_class;
    $image_alt = $this->image_alt;
    $weight = $this->weight;
    $width = $this->width;
    $height = $this->height;
    $is_active = ($this->is_active) ? 'TRUE' : 'FALSE';
    $link_url = $this->link_url;
    $link_title = $this->link_title;

    $query = db_query("INSERT INTO {simple_slideshow_images}
              (delta, image_name, image_src, image_class, image_alt, 
              weight, width, height, is_active, link_url) 
           VALUES (%d, '%s', '%s', '%s','%s',
               %d, %d, %d, '%s', '%s')",
    $delta, $image_name, $image_src, $image_class, $image_alt,
    $weight, $width, $height, $is_active, $link_url);
  }
}